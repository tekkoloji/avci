from datetime import datetime
from datetime import timedelta
from client import Client
import time
import sabit
import requests
import json
from threading import Thread


class Avci:
    def __init__(self):
        self.arsiv = {}
        self.client = None
        self.avlananlar = []
        self.listeGuncelle()

        self.analizZamani = datetime.now()
        self.temizlemeZamani = datetime.now()
        self.dataGondermeZamani = datetime.now()
        self.listeGuncellemeZamani = datetime.now()

        self.baglan()

    def baglan(self):
        while True:
            try:
                self.client = Client("", "")
                break
            except:
                print("Binance'e bağlanılamadı.")
                time.sleep(2)

    def listeGuncelle(self):
        while(True):
            try:
                semboller = requests.get(url=sabit.listeUrl).json()["symbols"]
                for sembol in semboller:
                    sAdi = sembol["symbol"]
                    sAktifMi = sembol["status"] == "TRADING"
                    sSpotMu = "SPOT" in sembol["permissions"]

                    if(sAktifMi and sSpotMu and (sAdi[-4:] in sabit.pariteListesi)):
                        if(not sAdi in self.arsiv):
                            self.arsiv[sAdi] = []
                self.listeGuncellemeZamani = datetime.now()
                break
            except:
                time.sleep(3)
                print("liste güncelleme hatası")

    def isle(self):
        piyasalar = self.client.get_all_tickers()
        print('.', end='', flush=True)
        for piyasa in piyasalar:
            sembol = piyasa["symbol"]
            fiyat = float(piyasa["price"])

            zaman = int(datetime.now().strftime("%Y%m%d%H%M%S"))
            piyasaFiyatItem = {"zaman": zaman, "fiyat": fiyat}
            if(sembol in self.arsiv):
                self.arsiv[sembol].append(piyasaFiyatItem)

    def temizlemeZamanFarki(self):
        return (datetime.now() - self.temizlemeZamani).seconds

    def analizZamanFarki(self):
        return (datetime.now() - self.analizZamani).seconds

    def dataGondermeZamanFarki(self):
        return (datetime.now() - self.dataGondermeZamani).seconds

    def listeGuncellemeZamanFarki(self):
        return (datetime.now() - self.listeGuncellemeZamani).seconds

    def analiz(self):
        self.avlananlar = []
        for piyasa in self.arsiv:
            sonuc = self.simulasyon(piyasa, self.arsiv[piyasa])
            self.avlananlar.append(sonuc)
            self.avlananlar.sort(
                key=lambda x: x["donemSayisi"], reverse=True)
        print('a', end='', flush=True)
        self.analizZamani = datetime.now()
        print(len(self.arsiv["BTCUSDT"]))

    def temizle(self):
        try:
            zamanSiniri = datetime.now() - timedelta(minutes=sabit.gecmisDk)
            zamanSiniri = int(zamanSiniri.strftime("%Y%m%d%H%M%S"))

            for piyasa in self.arsiv:
                while True:
                    if(len(self.arsiv[piyasa]) > 0):
                        # Boş değilse
                        if(self.arsiv[piyasa][0]["zaman"] < zamanSiniri):
                            self.arsiv[piyasa].pop(0)
                        else:
                            break
                    else:
                        # Boşsa Map elemanını sil
                        del self.arsiv[piyasa]
                        break
            self.temizlemeZamani = datetime.now()
            print('t', end='', flush=True)
        except:
            print("!temizleme hatası!")

    def simulasyon(self, piyasaAdi, zamanFiyatListesi):
        donemSayisi = 0
        alimSayisi = 0
        origin = 0.0

        ilkAlim = 100.0
        alinacakMiktar = 0.0

        mevcutAdet = 0.0
        mevcutMaliyet = 0.0
        alimYuzdeCizgisi = 1.0
        katSayi = 1.1
        alimPeriyodu = 0.01
        satimPeriyodu = 0.0065
        maximumAlim = 0
        for zamanFiyat in zamanFiyatListesi:
            fiyat = zamanFiyat["fiyat"]
            if(alimSayisi == 0):
                mevcutAdet = ilkAlim
                mevcutMaliyet = fiyat
                alimSayisi = 1
                origin = fiyat
                alinacakMiktar = alinacakMiktar * katSayi
            else:
                if(fiyat > mevcutMaliyet*(1+satimPeriyodu)):
                    donemSayisi = donemSayisi + 1
                    if(maximumAlim < alimSayisi):
                        maximumAlim = alimSayisi
                    alimSayisi = 0
                    mevcutAdet = 0.0
                    mevcutMaliyet = 0.0
                    alinacakMiktar = 100.0
                    origin = 0.0
                    alimYuzdeCizgisi = 1.0
                    # Satım

                elif(fiyat < origin*(alimYuzdeCizgisi-alimPeriyodu)):
                    mevcutMaliyet = (
                        mevcutMaliyet*mevcutAdet+alinacakMiktar*fiyat)/(mevcutAdet+alinacakMiktar)
                    mevcutAdet = mevcutAdet + alinacakMiktar
                    alinacakMiktar = alinacakMiktar*katSayi
                    alimSayisi = alimSayisi+1
                    alimYuzdeCizgisi = alimYuzdeCizgisi - alimPeriyodu

        result = {
            "sembol": piyasaAdi,
            "donemSayisi": donemSayisi,
            "maxAlimSayisi": maximumAlim
        }
        return result

    def dataGonder(self):
        try:
            print('d', end='', flush=True)
            usdt = "|"
            busd = "|"
            metin = "%s,%s,%s|"
            for item in self.avlananlar:
                hazir = metin % (item["sembol"], item["donemSayisi"],
                                 item["maxAlimSayisi"])

                usdTipi = item["sembol"][-4:]
                if usdTipi == "USDT":
                    usdt = usdt + hazir
                elif usdTipi == "BUSD":
                    busd = busd + hazir

            trSaati = self.turkiyeSaati().strftime("%Y.%m.%d %H:%M:%S")
            data = {"usdt": usdt, "busd": busd, "sonGuncelleme": trSaati}
            data = json.dumps(data)
            guncelle = sabit.apiLink + "?action=update&table=data&id=1&data="+data
            self.dataGondermeZamani = datetime.now()
            Thread(target=requests.get, args=[guncelle]).start()
        except:
            print("!data gönderme hatası!")

    def turkiyeSaati(self):
        simdi = datetime.utcnow() + timedelta(hours=3)
        return simdi

    def avla(self):
        while(True):
            try:
                self.isle()

                if self.analizZamanFarki() > 60:
                    self.analiz()

                if self.dataGondermeZamanFarki() > 60:
                    self.dataGonder()

                if self.temizlemeZamanFarki() > 60:
                    self.temizle()

                if self.listeGuncellemeZamanFarki() > 300:
                    self.listeGuncelle()
            except Exception as e:
                print(e)
                print("!avla hatası!")
            time.sleep(1)
